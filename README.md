Installer python 3 via apt-get
Installer python-pip via apt-get

Cloner le dépot du projet : git clone https://gitlab.com/thomas.graviou/gestion-adherents.git

Installer virtualenv via pip (pip install virtualenv)
Créer un environnement virtuel avec python3 comme interpréteur. Habituellement, cela se fait avec cette commande :
virtualenv venv -p /usr/bin/python3
Pour trouver le chemin de python3, utiliser "whereis python3"

Activer l'environnement virtuel : source venv/bin/activate
Installer les dépendances : pip install -r requirements.txt

Lancer l'application : python manage.py runserver

Pour développer, j'aime utiliser Visual Studio Code, un éditeur opensource, avec les extensions suivantes :
- Django
- Python