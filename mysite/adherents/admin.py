from django.contrib import admin

from .models import Adherent

admin.site.register(Adherent)
