from django.db import models
import datetime

class AdherentManager(models.Manager):
    def create_adherent(self, nom, prenom, date_naissance, adhesion, date_adhesion):
        
        try:
            date_fin = datetime.date(date_adhesion.year + 1, date_adhesion.month, date_adhesion.day)

        except ValueError:
            # On tombe ici si on essaie de créer un 29 février sur une année qui n'en contient pas
            date_fin = datetime.date(date_adhesion.year + 1, 3, 1)

        print(date_adhesion)
        print(date_fin)

        return self.create(nom=nom, prenom=prenom, adhesion=adhesion, date_adhesion=date_adhesion, date_fin=date_fin, date_naissance=date_naissance)

# Create your models here.
class Adherent(models.Model):
    objects = AdherentManager()

    nom = models.CharField(max_length=200)
    prenom = models.CharField(max_length=200)
    date_naissance = models.DateField('Date de naissance')

    A = 'A'
    B = 'B'
    C = 'C'
    D = 'D'
    E = 'E'
    F = 'F'
    G = 'G'

    adhesion = models.CharField(max_length=1, choices=[
        (A, 'A'),
        (B, 'B'),
        (C, 'C'),
        (D, 'D'),
        (E, 'E'),
        (F, 'F'),
        (G, 'G')
    ]) 
    date_adhesion = models.DateField('Debut adhésion')
    date_fin = models.DateField('Fin adhésion')
    

    def __str__(self):
        return self.prenom + ' ' +  self.nom
