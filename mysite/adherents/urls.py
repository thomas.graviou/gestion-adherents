from django.urls import path

from . import views

app_name = 'adherents'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:adherent_id>/', views.detail, name='detail'),
    path('create/', views.create, name='create'),
    path('register/', views.register_adherent, name='register'),
    path('register/<int:adherent_id>', views.update_adherent, name='update'),
]

