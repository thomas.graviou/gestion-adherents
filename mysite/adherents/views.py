from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django import forms

from .models import Adherent
from .forms import AdherentForm

import datetime

def index(request):
    adherents = Adherent.objects.all()
    context = {
        'adherents': adherents
    }
    return render(request, 'adherents/index.html', context)

def register_adherent(request):
    nom = request.POST['nom']
    prenom = request.POST['prenom']
    adhesion = request.POST['adhesion']
    debut_adhesion = datetime.datetime.now()
    date_naissance = request.POST['date_naissance']

    adherent = Adherent.objects.create_adherent(nom, prenom, date_naissance, adhesion, debut_adhesion)
    return HttpResponseRedirect(reverse('adherents:detail', args=(adherent.id,)))

def update_adherent(request, adherent_id):
    nom = request.POST['nom']
    prenom = request.POST['prenom']
    adhesion = request.POST['adhesion']
    debut_adhesion = datetime.datetime.now()
    date_naissance = request.POST['date_naissance']

    adherent = get_object_or_404(Adherent, pk=adherent_id)

    adherent.nom = nom
    adherent.prenom = prenom
    adherent.save()

    return HttpResponseRedirect(reverse('adherents:detail', args=(adherent.id,)))

def create(request):
    form = AdherentForm()
    url = reverse('adherents:register')

    return render(request, 'adherents/detail.html', { 'form': form,
                                                        'url': url })

def detail(request, adherent_id):
    adherent = get_object_or_404(Adherent, pk=adherent_id)

    form = AdherentForm(instance=adherent)
    url = reverse('adherents:update', args=(adherent_id,))

    return render(request, 'adherents/detail.html', { 'adherent': adherent,
                                                        'url': url,
                                                        'form': form })